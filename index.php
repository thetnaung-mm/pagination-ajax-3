<html>
<head>
<link href="pagination_style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="jquery-3.2.1.min.js"></script>

</head>
<body>
<?php 

$con = mysqli_connect("localhost", "root", "", "ajax_pagination");

$select = mysqli_query($con, "SELECT * FROM language");
$total_results = mysqli_num_rows($select);
$row = mysqli_fetch_array($select, MYSQLI_ASSOC);
?>

	<div id="ajax-content-container">
		<p class="para1"><?php echo $row['language_name'];?></p>
		<p class="para2"><?php echo $row['language_description'];?></p>
	</div>

	<div id="tota_page_div">
		<?php
		for($i = 1; $i <= $total_results; $i++)
		{
			echo "<input type='button' value='".$i."' onclick='get_data(".$i.")'>";
		}
		?>
	</div>


	<script type="text/javascript">
	function get_data(no)
	{
	 	$.ajax ({
	  		type:'post',
	  		url:'get_data.php',
	  		data:{
	   			row_no:no
	  		},
	  		success:function(response) {
	   			document.getElementById("ajax-content-container").innerHTML=response;
	  		}
	 	});
	}
	</script>
</body>
</html>